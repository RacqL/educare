const knex = require(`knex`)
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const db = knex ({
    client: 'mysql',
    connection: {
        host: 'localhost',
        database: 'hackathon',
        user: 'root',
        password: '',
    }
})

const app = express()

app  
    .use(bodyParser.json())
    .use(cors())
    .get('/api/people', (request, response) => {
        db
            .select()
            .from('people')
            .asCallback((error, result) => {
                response.json(result)
            })
    })

    .listen(5000, () => {
        console.log('ari na daog na kami')
    })