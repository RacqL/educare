import firebase from 'firebase'

const firebaseConfig = {
  apiKey: 'AIzaSyB8TPcl2EKi9iTZF71rxrMipoeHNXtgZ_c',
  authDomain: 'fir-web-learn-10c82.firebaseapp.com',
  databaseURL: 'https://fir-web-learn-10c82.firebaseio.com',
  projectId: 'fir-web-learn-10c82',
  storageBucket: 'fir-web-learn-10c82.appspot.com',
  messagingSenderId: '771523841614',
  appId: '1:771523841614:web:91d9d9611c7498d1'
}

export default firebase.initializeApp(firebaseConfig)
