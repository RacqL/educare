
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/Index.vue') },
      { path: '/people', component: () => import('pages/People.vue') },
      { path: '/upload', component: () => import('pages/ImageUpload.vue') },
      { path: '/signup', component: () => import('pages/SignUp.vue') },
      { path: '/login', component: () => import('pages/LogIn.vue') },
      { path: '/course', component: () => import('pages/Course.vue') },
      { path: '/addstudentcourse', component: () => import('pages/AddStudentCourse.vue') },
      { path: '/newsignup', component: () => import('pages/NewSignUp.vue') },
      { path: '/dashboard', component: () => import('pages/Dashboard.vue') },
      { path: '/createCourse', component: () => import('pages/CreateCourse.vue') },
      { path: '/subject', component: () => import('pages/subject.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
